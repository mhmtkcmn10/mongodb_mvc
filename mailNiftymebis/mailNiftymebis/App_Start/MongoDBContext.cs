﻿using mailNiftymebis.Entities;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace mailNiftymebis.App_Start
{
    public class MongoDBContext
    {
        public IMongoDatabase database;
        public IMongoCollection<UserLogin> userLoginCollection ;
        public IMongoCollection<Mail> mailCollection;
        public IMongoCollection<MailDetails> mailDetailsCollection ;
        
        public MongoDBContext()
        {
            var mongoclient = new MongoClient(ConfigurationManager.AppSettings["mongoDBHost"]);
            database = mongoclient.GetDatabase(ConfigurationManager.AppSettings["mongoDBName"]);

            userLoginCollection = database.GetCollection<UserLogin>("User");
            mailCollection = database.GetCollection<Mail>("Mail");
            mailDetailsCollection = database.GetCollection<MailDetails>("MailDetails");

        }




    }
}