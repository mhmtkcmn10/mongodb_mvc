﻿using mailNiftymebis.Entities;
using mailNiftymebis.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mailNiftymebis.Controllers
{
    public class MailComposeController : Controller
    {
        private Mail mail = new Mail();
        private UserLogin userlogin = new UserLogin();
        private MailDetails mailDetails = new MailDetails();

        private MailModel mailModel = new MailModel();
        private MailDetailsModel mailDetailsModel = new MailDetailsModel();
        private UserLoginModel userLoginModel = new UserLoginModel();
        public void DropListele()
        {
            var result = userLoginModel.findAll();

            ViewBag.loginList = result;
        }

        // GET: MailCompose
        public ActionResult MailCompose()
        {
            DropListele();

            if (Session["Id"] == null)
            {
                return RedirectToAction("UserLogin","UserLogin");
            }
            string deger = Session["Id"].ToString();
            var gondID = new ObjectId(deger);
            var modelRead = mailDetailsModel.findLookup().Where(x => x.Takerid == gondID && x.StatusREAD == false).ToList();

            ViewBag.readCount = modelRead.Count();
            return View();
        }

        [HttpPost]
        public JsonResult MailCompose(string title, string message, string[] taker)
        {
            if (taker == null || message==null || title==null)
            {
                DropListele();
                ViewBag.CreateSendError = "Hata oluştu lütfen kontrol ediniz ...";
                return Json(new { Success = false }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                mail.Title = title;
                mail.Message = message;
                string tarih = DateTime.Now.ToString();
                string deger = Session["Id"].ToString();
                var gondid = new ObjectId(deger);
                mail.Date = Convert.ToDateTime(tarih);
                mail.Senderid = gondid;
                mail.SenderStatus = false;

                mailModel.create(mail);

                var mailID = mail.Id;
                mailDetails.Mailid = mailID;
                mailDetails.Senderid = gondid;

                var mailIDsifir = new ObjectId();
                
                foreach (var item in taker)
                {
                    mailDetails.Takerid = new ObjectId(item);
                    mailDetailsModel.create(mailDetails);
                    mailDetails.Id = mailIDsifir;
                    
                }
                DropListele();
                ViewBag.CreateSendSuccess = "Mesajiniz gonderildi ...";
                return Json(new { Success = true }, JsonRequestBehavior.AllowGet);


            }


        }
    }
}