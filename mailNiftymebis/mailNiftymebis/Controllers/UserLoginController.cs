﻿using mailNiftymebis.Entities;
using mailNiftymebis.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mailNiftymebis.Controllers
{
    public class UserLoginController : Controller
    {
        private UserLoginModel userloginModel = new UserLoginModel();

        // GET: UserLogin
        public ActionResult UserLogin()
        {
            return View();
        }
        [HttpPost]
        public JsonResult UserLogin(string username, string password)
        {
            //System.Threading.Thread.Sleep(1500);  Zamanlayıcı
            var userObje = userloginModel.findAll()
                                 .Where(x => x.Username == username &&
                                             x.Password == password).FirstOrDefault();
            if (userObje == null)
            {
                ViewBag.LoginError = "Hatalı Giriş yaptınız ... Kontrol ediniz ...";
                return Json(new { Success = false }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                Session["Id"] = userObje.Id;
                Session["Name"] = userObje.Name;
                Session["Surname"] = userObje.Surname;
                Session["Email"] = userObje.Email;

                return Json(new { Success = true }, JsonRequestBehavior.AllowGet);
            }

        }
    }
}