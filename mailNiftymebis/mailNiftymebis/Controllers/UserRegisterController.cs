﻿using mailNiftymebis.Entities;
using mailNiftymebis.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mailNiftymebis.Controllers
{
    public class UserRegisterController : Controller
    {
        private UserLoginModel userloginModel = new UserLoginModel();

        // GET: UserRegister
        public ActionResult UserRegister()
        {
            return View();
        }
        [HttpPost]
        public JsonResult UserRegister(string name, string surname, string email, string username, string password)
        {
            if (name == "" || surname == "" || email == "" || username == "" || password == "")
            {
                return Json(new { Success = false }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                var userObje = userloginModel.findAll().Where(x => x.Email == email || x.Username == username).Count();

                if (userObje>0)
                {
                    return Json(new { Success = false }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    UserLogin userLogin = new UserLogin();

                    userLogin.Name = name;
                    userLogin.Surname = surname;
                    userLogin.Email = email;
                    userLogin.Username = username;
                    userLogin.Password = password;


                    userloginModel.create(userLogin);

                    return Json(new { Success = true }, JsonRequestBehavior.AllowGet);
                }

               
            }

        }
    }
}