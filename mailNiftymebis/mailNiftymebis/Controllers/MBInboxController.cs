﻿using mailNiftymebis.Entities;
using mailNiftymebis.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mailNiftymebis.Controllers
{
    public class MBInboxController : Controller
    {
        private Mail mail = new Mail();
        private UserLogin userlogin = new UserLogin();
        private MailDetails mailDetails = new MailDetails();

        private MailModel mailModel = new MailModel();
        private MailDetailsModel mailDetailsModel = new MailDetailsModel();
        private UserLoginModel userLoginModel = new UserLoginModel();

        // GET: MBInbox
        public ActionResult MBInbox(string Sorting_Order, int? Page_No)/*int page = 1, int pageSize = 10*/
        {
            if (Session["Id"] == null)
            {
                return RedirectToAction("UserLogin", "UserLogin");
            }
            string deger = Session["Id"].ToString();
            var gondID = new ObjectId(deger);
            var modelDetail = mailDetailsModel.findLookup().ToList();

            ViewBag.CurrentSortOrder = Sorting_Order;
            ViewBag.SortingDate = Sorting_Order == null || Sorting_Order == "near_date" ? "far_date" : "near_date";

            switch (Sorting_Order)
            {
                case "far_date":
                    modelDetail = mailDetailsModel.findLookup().Where(x => x.Takerid == gondID && x.TakerSTATUS == false).ToList();
                    break;
                case "near_date":
                    modelDetail = mailDetailsModel.findLookup().Where(x => x.Takerid == gondID && x.TakerSTATUS == false).OrderByDescending(x => x.Mails[0].Date).ToList();
                    break;
                default:
                    modelDetail = mailDetailsModel.findLookup().Where(x => x.Takerid == gondID && x.TakerSTATUS == false).OrderByDescending(x => x.Mails[0].Date).ToList();
                    break;
            }
            var modelRead = mailDetailsModel.findLookup().Where(x => x.Takerid == gondID && x.StatusREAD == false && x.TakerSTATUS==false).ToList();
            ViewBag.readCount = modelRead.Count();

            int Size_Of_Page = 5;
            int No_Of_Page = (Page_No ?? 1);
            PagedList<MailDetails> detailsModel = new PagedList<MailDetails>(modelDetail, No_Of_Page, Size_Of_Page);

            return View(detailsModel);
        }
        [HttpGet]
        public ActionResult MBInboxMES(string id)
        {
            if (Session["Id"] == null)
            {
                return RedirectToAction("UserLogin", "UserLogin");
            }
            var userResult = userLoginModel.findAll();

            ViewBag.loginList = userResult;

            var currentAccount = mailDetailsModel.find(id);

            string deger = Session["Id"].ToString();
            var gondID = new ObjectId(deger);
            var messageID = new ObjectId(id);

            var result = mailDetailsModel.findLookup().Where(x => x.Takerid == gondID && x.Id == messageID).OrderByDescending(x => x.Mails[0].Date).ToList();

            ViewBag.mailbilgi = result;
            var modelRead = mailDetailsModel.findLookup().Where(x => x.Takerid == gondID && x.StatusREAD == false).ToList();

            ViewBag.readCount = modelRead.Count();

            if (currentAccount.StatusREAD == false)
            {
                currentAccount.StatusREAD = true;
                mailDetailsModel.update(id, currentAccount);
            }
            return View("MBInboxMES", currentAccount);
        }
        public void DataDelete(string id)
        {
            var currentAccount = mailDetailsModel.find(id);
            currentAccount.TakerSTATUS = true;
            mailDetailsModel.update(id, currentAccount);
        }
        [HttpPost]
        public JsonResult MessageDelete(string id)
        {
            try
            {
                if (id != null)
                {
                    DataDelete(id);
                    return Json(new { Success = true }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Success = false }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (ArgumentNullException ne)
            {
                ne.ToString();
                return Json(new { Success = false }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult DeleteMail(string[] deletedItems)
        {
            try
            {
                if (deletedItems != null)
                {
                    foreach (string inboxID in deletedItems)
                    {
                        DataDelete(inboxID);

                    }
                    return Json(new { Success = true }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Success = false }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (ArgumentNullException ne)
            {
                ne.ToString();
                return Json(new { Success = false }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult MailUnRead(string[] updatedItems)
        {
            try
            {
                if (updatedItems != null)
                {
                    foreach (string inboxID in updatedItems)
                    {
                        var currentAccount = mailDetailsModel.find(inboxID);
                        currentAccount.StatusREAD = false;
                        mailDetailsModel.update(inboxID, currentAccount);
                    }
                    return Json(new { Success = true }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Success = false }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (ArgumentNullException ne)
            {
                ne.ToString();
                return Json(new { Success = false }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult MailRead(string[] updatedItems)
        {
            try
            {
                if (updatedItems != null)
                {
                    foreach (string inboxID in updatedItems)
                    {
                        var currentAccount = mailDetailsModel.find(inboxID);
                        currentAccount.StatusREAD = true;
                        mailDetailsModel.update(inboxID, currentAccount);
                    }
                    return Json(new { Success = true }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Success = false }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (ArgumentNullException ne)
            {
                ne.ToString();
                return Json(new { Success = false }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult MailInboxCompose(string title, string message, string[] taker)
        {
            if (taker == null || message == null || title == null)
            {
                ViewBag.CreateSendError = "Hata oluştu lütfen kontrol ediniz ...";
                return Json(new { Success = false }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                mail.Title = title;
                mail.Message = message;
                string tarih = DateTime.Now.ToString();
                string deger = Session["Id"].ToString();
                var gondid = new ObjectId(deger);
                mail.Date = Convert.ToDateTime(tarih);
                mail.Senderid = gondid;
                mail.SenderStatus = false;

                mailModel.create(mail);
                var mailID = mail.Id;
                mailDetails.Mailid = mailID;
                mailDetails.Senderid = gondid;
                var sifir2 = new ObjectId();

                foreach (var item in taker)
                {
                    mailDetails.Takerid = new ObjectId(item);
                    mailDetailsModel.create(mailDetails);
                    mailDetails.Id = sifir2;
                }
                ViewBag.CreateSendSuccess = "Mesajiniz gonderildi ...";
                return Json(new { Success = true }, JsonRequestBehavior.AllowGet);

            }


        }



    }
}
