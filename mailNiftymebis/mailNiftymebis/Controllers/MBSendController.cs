﻿using mailNiftymebis.App_Start;
using mailNiftymebis.Entities;
using mailNiftymebis.Models;
using Microsoft.Ajax.Utilities;
using MongoDB.Bson;
using MongoDB.Driver;
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mailNiftymebis.Controllers
{
    public class MBSendController : Controller
    {
        private MongoDBContext dBContext = new MongoDBContext();

        private Mail mail = new Mail();
        private UserLogin userlogin = new UserLogin();
        private MailDetails mailDetails = new MailDetails();

        private MailModel mailModel = new MailModel();
        private MailDetailsModel mailDetailsModel = new MailDetailsModel();
        private UserLoginModel userLoginModel = new UserLoginModel();

        // GET: MBSend
        public ActionResult MBSend(string Sorting_Order, int? Page_No)/*int page = 1, int pageSize = 10*/
        {
            #region PagedVeriListeleme
            //if (Session["Id"] == null)
            //{
            //    return RedirectToAction("UserLogin", "UserLogin");
            //}
            ////Sessionda ki idye göre veri listeleme
            //string deger = Session["Id"].ToString();
            //var gondID = new ObjectId(deger);

            //var reso = mailDetailsModel.findLookup().Where(x => x.Senderid == gondID && x.SenderSTATUS == false).OrderByDescending(x => x.Mails[0].Date).ToList();

            //var result = reso.AsQueryable()
            //    .GroupBy(s => new { s.Mailid, s.Mails[0].Title, s.Mails[0].Message, s.Mails[0].Date })
            //    .Select(y => new Child { Mailid = y.Key.Mailid, Title = y.Key.Title, Message = y.Key.Message, Date = y.Key.Date, MailDetails = y.ToList() });

            ////ViewBag.mails = result;
            //var modelRead = mailDetailsModel.findLookup().Where(x => x.Takerid == gondID && x.StatusREAD == false).ToList();
            //ViewBag.readCount = modelRead.Count();
            //PagedList<Child> detailsModel = new PagedList<Child>(result, page, pageSize);

            //return View(detailsModel);
            #endregion
            if (Session["Id"] == null)
            {
                return RedirectToAction("UserLogin", "UserLogin");
            }
            string deger = Session["Id"].ToString();
            var gondID = new ObjectId(deger);
            var modelDetail = mailDetailsModel.findLookup().ToList();
            ViewBag.CurrentSortOrder = Sorting_Order;
            ViewBag.SortingDate = Sorting_Order == null || Sorting_Order == "near_date" ? "far_date" : "near_date";

            switch (Sorting_Order)
            {
                case "far_date":
                    modelDetail = mailDetailsModel.findLookup().Where(x => x.Senderid == gondID && x.SenderSTATUS == false).ToList();
                    break;
                case "near_date":
                    modelDetail = mailDetailsModel.findLookup().Where(x => x.Senderid == gondID && x.SenderSTATUS == false).OrderByDescending(x => x.Mails[0].Date).ToList();
                    break;
                default:
                    modelDetail = mailDetailsModel.findLookup().Where(x => x.Senderid == gondID && x.SenderSTATUS == false).OrderByDescending(x => x.Mails[0].Date).ToList();
                    break;
            }

            var result = modelDetail.AsQueryable()
                .GroupBy(s => new { s.Mailid, s.Mails[0].Title, s.Mails[0].Message, s.Mails[0].Date })
                .Select(y => new Child { Mailid = y.Key.Mailid, Title = y.Key.Title, Message = y.Key.Message, Date = y.Key.Date, MailDetails = y.ToList() });
            var modelRead = mailDetailsModel.findLookup().Where(x => x.Takerid == gondID && x.StatusREAD == false).ToList();
            ViewBag.readCount = modelRead.Count();
            int Size_Of_Page = 5;
            int No_Of_Page = (Page_No ?? 1);

            PagedList<Child> detailsModel = new PagedList<Child>(result, No_Of_Page, Size_Of_Page);
            return View(detailsModel);
        }

        [HttpGet]
        public ActionResult MBSendMES(string id/*, string takername, string takersurname, string takeremail*/)
        {
            if (Session["Id"] == null)
            {
                return RedirectToAction("UserLogin", "UserLogin");
            }
            var result = userLoginModel.findAll();
            ViewBag.loginList = result;
            //var currentAccount = mailDetailsModel.find(id);

            var currentAccount = mailModel.find(id);
            string deger = Session["Id"].ToString();
            var gondID = new ObjectId(deger);

            var ca = new ObjectId(id);


            var resultDATA = mailDetailsModel.findLookup().Where(x => x.Senderid == gondID && x.Mailid == ca).OrderByDescending(x => x.Mails[0].Date).ToList();

            ViewBag.result = resultDATA;

            var takerResult = resultDATA.AsQueryable().Where(x => x.Mailid == ca)
                .GroupBy(s => new { s.Mailid, s.Mails[0].Title, s.Mails[0].Message, s.Mails[0].Date })
                .Select(y => new Child { Mailid = y.Key.Mailid, Title = y.Key.Title, Message = y.Key.Message, Date = y.Key.Date, MailDetails = y.ToList() });

            ViewBag.takerInfo = takerResult;
            var modelRead = mailDetailsModel.findLookup().Where(x => x.Takerid == gondID && x.StatusREAD == false).ToList();

            ViewBag.readCount = modelRead.Count();
            //currentAccount.TakerName = takername;
            //currentAccount.TakerSurname = takersurname;
            //currentAccount.TakerEmail = takeremail;

            return View("MBSendMES", currentAccount);
        }

        public void DataDelete(string id)
        {
            var docId = new ObjectId(id);

            List<MailDetails> details = dBContext.mailDetailsCollection.AsQueryable<MailDetails>().Where(x => x.Mailid == docId).ToList();
            var test = details;

            foreach (var item in test)
            {
                item.SenderSTATUS = true;
                var ID = item.Id;

                string rowID = ID.ToString();

                mailDetailsModel.update(rowID, item);
            }
        }

        [HttpPost]
        public JsonResult DeleteMessage(string id)
        {
            try
            {
                if (id != null)
                {
                    DataDelete(id);

                    return Json(new { Success = true }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Success = false }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (ArgumentNullException ne)
            {
                ne.ToString();
                return Json(new { Success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DeleteMailSend(string[] deletedItems)
        {
            try
            {
                if (deletedItems != null)
                {
                    foreach (string id in deletedItems)
                    {
                        DataDelete(id);

                    }
                    return Json(new { Success = true }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Success = false }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (ArgumentNullException ne)
            {
                ne.ToString();
                return Json(new { Success = false }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult MailSendCompose(string title, string message, string[] taker)
        {
            if (taker == null || message == null || title == null)
            {
                ViewBag.CreateSendError = "Hata oluştu lütfen kontrol ediniz ...";
                return Json(new { Success = false }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                mail.Title = title;
                mail.Message = message;
                string tarih = DateTime.Now.ToString();
                string deger = Session["Id"].ToString();
                var gondid = new ObjectId(deger);
                mail.Date = Convert.ToDateTime(tarih);
                mail.Senderid = gondid;
                mail.SenderStatus = false;

                mailModel.create(mail);
                var mailID = mail.Id;
                mailDetails.Mailid = mailID;
                mailDetails.Senderid = gondid;
                var zeroId = new ObjectId();
                //mailDetails.Parentid = firstID;

                foreach (var item in taker)
                {
                    mailDetails.Takerid = new ObjectId(item);
                    mailDetailsModel.create(mailDetails);
                    mailDetails.Id = zeroId;
                }
                ViewBag.CreateSendSuccess = "Mesajiniz gonderildi ...";
                return Json(new { Success = true }, JsonRequestBehavior.AllowGet);

            }

        }

    }
}