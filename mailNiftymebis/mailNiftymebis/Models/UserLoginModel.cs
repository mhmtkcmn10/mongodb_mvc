﻿using mailNiftymebis.App_Start;
using mailNiftymebis.Entities;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace mailNiftymebis.Models
{
    public class UserLoginModel : IRepository<UserLogin>
    {
        private MongoDBContext dBContext = new MongoDBContext();

        public void create(UserLogin entity)
        {
            dBContext.userLoginCollection.InsertOne(entity);
        }

        public void delete(string id)
        {
            var docId = new ObjectId(id);
            dBContext.userLoginCollection.DeleteOne(m => m.Id == docId);
        }

        public UserLogin find(string id)
        {
            var docId = new ObjectId(id);
            return dBContext.userLoginCollection.Find<UserLogin>(m => m.Id == docId).FirstOrDefault();
        }


        public List<UserLogin> findAll()
        {
            return dBContext.userLoginCollection.AsQueryable<UserLogin>().ToList();
        }

        public void update(string id, UserLogin entity)
        {
            var docId = new ObjectId(id);
            dBContext.userLoginCollection.ReplaceOne(m => m.Id == docId, entity);
        }
    }
}