﻿using mailNiftymebis.App_Start;
using mailNiftymebis.Entities;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace mailNiftymebis.Models
{
    public class MailModel : IRepository<Mail>
    {
        private MongoDBContext dBContext = new MongoDBContext();

        public void create(Mail entity)
        {
            dBContext.mailCollection.InsertOne(entity);
        }

        public void delete(string id)
        {
            var docId = new ObjectId(id);
            dBContext.mailCollection.DeleteOne(m => m.Id == docId);
        }

        public Mail find(string id)
        {
            var docId = new ObjectId(id);
            return dBContext.mailCollection.Find<Mail>(m => m.Id == docId).FirstOrDefault();
        }

        public List<Mail> findAll()
        {
            return dBContext.mailCollection.AsQueryable<Mail>().ToList();
        }

        public void update(string id, Mail entity)
        {
            var docId = new ObjectId(id);
            dBContext.mailCollection.ReplaceOne(m => m.Id == docId, entity);
        }
    }
}