﻿
using mailNiftymebis.App_Start;
using mailNiftymebis.Entities;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace mailNiftymebis.Models
{
    public class MailDetailsModel:IRepository<MailDetails>
    {
        private MongoDBContext dBContext = new MongoDBContext();

        public void create(MailDetails entity)
        {
            dBContext.mailDetailsCollection.InsertOne(entity);
        }

        public void delete(string id)
        {
            var docId = new ObjectId(id);
            dBContext.mailDetailsCollection.DeleteOne(m=>m.Id==docId);
        }

        public MailDetails find(string id)
        {
            var docId = new ObjectId(id);
            return dBContext.mailDetailsCollection.Find<MailDetails>(m => m.Id == docId).FirstOrDefault();
        }

        public List<MailDetails> findAll()
        {
            return dBContext.mailDetailsCollection.AsQueryable<MailDetails>().ToList();
        }

        public void update(string id, MailDetails entity)
        {
            var docId = new ObjectId(id);
            dBContext.mailDetailsCollection.ReplaceOne(m=>m.Id==docId,entity);
        }
        
        public List<MailDetails> findLookup()
        {
            List<MailDetails> result = dBContext.mailDetailsCollection.Aggregate()
                .Lookup<MailDetails, UserLogin, MailDetails>(
                  foreignCollection: dBContext.userLoginCollection,
                  localField: e => e.Senderid,
                  foreignField: f => f.Id,
                  @as: (MailDetails eo) => eo.UserLogins)
                .Lookup(
                  foreignCollection: dBContext.userLoginCollection,
                  localField: e => e.Takerid,
                  foreignField: f => f.Id,
                  @as: (MailDetails eo) => eo.UserLogins2)
                  .Lookup(
                  foreignCollection: dBContext.mailCollection,
                  localField: e => e.Mailid,
                  foreignField: f => f.Id,
                  @as: (MailDetails eo) => eo.Mails)
                .ToList();

            return result;
        }

    }
}