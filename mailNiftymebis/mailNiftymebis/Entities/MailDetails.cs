﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mailNiftymebis.Entities
{
    public class MailDetails
    {
        [BsonId]
        public ObjectId Id
        {
            get;
            set;
        }
        //[BsonElement("parentID")]
        //[BsonRepresentation(BsonType.ObjectId)]
        //public string Parentid
        //{
        //    get;
        //    set;
        //}
        [BsonElement("mailid")]
        public ObjectId Mailid
        {
            get;
            set;
        }
        [BsonElement("senderID")]
        public ObjectId Senderid
        {
            get;
            set;
        }
        [BsonElement("takerID")]
        public ObjectId Takerid
        {
            get;
            set;
        }
        [BsonElement("statusREAD")]
        public bool StatusREAD
        {
            get;
            set;
        }
        [BsonElement("senderSTATUS")]
        public bool SenderSTATUS
        {
            get;
            set;
        }
        [BsonElement("takerSTATUS")]
        public bool TakerSTATUS
        {
            get;
            set;
        }
        //public string SenderName
        //{
        //    get;
        //    set;
        //}
        //public string SenderSurname
        //{
        //    get;
        //    set;
        //}
        //public string SenderEmail
        //{
        //    get;
        //    set;
        //}

        //public string TakerName
        //{
        //    get;
        //    set;
        //}
        //public string TakerSurname
        //{
        //    get;
        //    set;
        //}
        //public string TakerEmail
        //{
        //    get;
        //    set;
        //}


        public Mail[] Mails { get; set; }

        public UserLogin[] UserLogins { get; set; }

        public UserLogin[] UserLogins2 { get; set; }

        public Mail[] Parents { get; set; }


    }
}