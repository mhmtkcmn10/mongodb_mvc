﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mailNiftymebis.Entities
{
    public class Mail
    {
        [BsonId]
        public ObjectId Id
        {
            get;
            set;
        }

        [BsonElement("title")]
        public string Title
        {
            get;
            set;
        }

        [BsonElement("message")]
        [AllowHtml]
        public string Message
        {
            get;
            set;
        }

        [BsonElement("date")]
        public DateTime Date
        {
            get;
            set;
        }

        [BsonElement("senderID")]
        public ObjectId Senderid
        {
            get;
            set;
        }

        [BsonElement("senderSTATUS")]
        public bool SenderStatus
        {
            get;
            set;
        }












    }
}