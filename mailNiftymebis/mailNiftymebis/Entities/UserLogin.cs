﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace mailNiftymebis.Entities
{
    public class UserLogin
    {
        [BsonId]
        public ObjectId Id
        {
            get;
            set;
        }

        [BsonElement("name")]
        public string Name
        {
            get;
            set;
        }

        [BsonElement("surname")]
        public string Surname
        {
            get;
            set;
        }
        [BsonElement("email")]
        [DataType(DataType.EmailAddress)]
        public string Email
        {
            get;
            set;
        }

        [BsonElement("username")]
        public string Username
        {
            get;
            set;
        }

        [BsonElement("password")]
        public string Password
        {
            get;
            set;
        }

        [BsonElement("status")]
        public bool Status
        {
            get;
            set;
        }

    }
}